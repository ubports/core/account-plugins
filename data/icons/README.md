# Icon Sources and License/Copyright Information

Files: scalable/online-accounts-foursquare.svg  
 scalable/online-accounts-foursquare.png  
Source: gnome-online-accounts 3.36.0-1ubuntu1  
Copyright: 2011 Red Hat, Inc. (probably incorrect, taken from d/copyright of gnome-online-accounts)  
License: LGPL-2.1+

Files: scalable/online-accounts-identica.svg  
 online-accounts-identica.png  
Source: https://www.svgrepo.com/svg/378010/identica  
Copyright: JCubic (https://www.svgrepo.com/author/jcubic/)
License: [PD License](https://www.svgrepo.com/page/licensing/#PD)

Files: scalable/online-accounts-instagram.svg  
 online-accounts-instagram.png  
Source: deepin-icon-theme 15.12.71-1  
Copyright: 2015-2018 Deepin Technology Co., Ltd. <support@deepin.org>  
License: LGPL-3+

Files: online-accounts-linkedin.png  
Copyright: 2016,Tianjin KYLIN Information Technology Co., Ltd.  
License: GPL-3

Files: online-accounts-mcloud.png  
 online-accounts-microsoft.png  
 online-accounts-nextcloud.png  
 online-accounts-owncloud.png  
Copyright: 2012, Canonical Ltd.  
License: GPL-2+

Files: online-accounts-sina.png  
 online-accounts-sohu.png  
Source: ubuntu-kylin-software-center 3.3.3  
Copyright: 2013-2014, National University of Defense Technology(NUDT) & Kylin Ltd.  
License: GPL-3

Files: scalable/online-accounts-vk.svg  
 scalable/online-accounts-vk.png  
Source: papirus-icon-theme 20200201-1  
Copyright: 2016-2017 Alexey Varfolomeev <varlesh@gmail.com>  
           2016-2017 Sergei Eremenko <finalitik@gmail.com>  
           2016-2017 Papirus Development Team  
License: GPL-3  
Comment:  
 Original sources:  
  - Paper Icon Theme (https://github.com/snwh/paper-icon-theme)  
    Author: Sam Hewitt (https://samuelhewitt.com)  
    License: LGPL-3 (https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/issues/1049#issuecomment-356642584)  
  - Numix icon theme (https://github.com/numixproject/numix-icon-theme)  
    Author: Numix Project (http://numixproject.org)  
    License: GPL-3  
  - Numix Core (https://github.com/numixproject/numix-core)  
    Author: Numix Project (http://numixproject.org)  
    License: GPL-3  
  - elementary+ Icon Theme (https://github.com/mank319/elementaryPlus)  
    Author: elementaryPlus contributors (https://github.com/mank319/elementaryPlus)  
    License: GPL-3  

Files: online-accounts-x.png  
Source: https://commons.wikimedia.org/wiki/File:X_logo_twitter_new_brand_icon.svg  
Copyright: 2024, TobiasKa  
License: CC0-1.0 Universal Public Domain Dedication
